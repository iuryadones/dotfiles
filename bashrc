#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi
shopt -s autocd

alias dirs='dirs -v'
alias grep='grep --color=auto'
alias ls='ls --color=auto'

PS1=' \W \$ '

if [ -d "$HOME/.pyenv" ]
then
    export PYENV_ROOT="$HOME/.pyenv"
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

[[ -d "$HOME/.cargo" ]] && . "$HOME/.cargo/env"
