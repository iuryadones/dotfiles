#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# vim
[[ -f ~/.vimrc ]] && mv ~/.vimrc ~/.vimrc.bkp
ln -s ${BASEDIR}/vimrc ~/.vimrc

# bash
[[ -f ~/.bashrc ]] && mv ~/.bashrc ~/.bashrc.bkp
ln -s ${BASEDIR}/bashrc ~/.bashrc

# input
[[ -f ~/.inputrc ]] && mv ~/.inputrc ~/.inputrc.bkp
ln -s ${BASEDIR}/inputrc ~/.inputrc

# git
[[ -f ~/.gitconfig ]] && mv ~/.gitconfig ~/.gitconfig.bkp
ln -s ${BASEDIR}/gitconfig ~/.gitconfig

# tmux
[[ -f ~/.tmux.conf ]] && mv ~/.tmux.conf ~/.tmux.conf.bkp
ln -s ${BASEDIR}/tmux.conf ~/.tmux.conf

# xterm
[[ -f ~/.Xdefaults ]] && mv ~/.Xdefaults ~/.Xdefaults.bkp
ln -s ${BASEDIR}/Xdefaults ~/.Xdefaults
