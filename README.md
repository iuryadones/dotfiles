# DotFiles

```
.
├── bashrc
├── gitconfig
├── inputrc
├── README.md
├── setup.sh
├── tmux.conf
├── vimrc
├── work
│   └── gitconfig
└── Xdefaults
```

## Setup

```bash
$ cd ~
$ git clone https://gitlab.com/iuryadones/dotfiles .files
$ cd .files
$ ./setup.sh
```
