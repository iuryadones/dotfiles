filetype plugin indent on
syntax enable

set nocompatible

set expandtab
set shiftwidth=4
set softtabstop=0
set tabstop=4

set autoread
set smartindent
set ignorecase
set cursorline

set hls
set list
set listchars=tab:→\ ,nbsp:␣,space:·,trail:•,eol:⏎,precedes:«,extends:»

set number
set relativenumber
set laststatus=2
set showtabline=2
set scrolloff=1

set t_Co=256
set background=dark

set textwidth=80
set colorcolumn=81
highlight ColorColumn ctermbg=8 guibg=darkgrey

let g:netrw_bufsettings = "noma nomod nu nobl nowrap ro rnu"
